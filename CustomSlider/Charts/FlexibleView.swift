//
//  FlexibleView.swift
//  CustomSlider
//
//  Created by Maxim on 14.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit

class FlexibleView: UIView {
    
    init(frame: CGRect, colorTheme: ColorTheme) {
        super.init(frame: frame)
        
        isUserInteractionEnabled = false
        contentMode = .redraw
        translatesAutoresizingMaskIntoConstraints = false
        
        switch colorTheme {
        case .day:
            backgroundColor = ColorScheme.lightenMask.day()
        case .night:
            backgroundColor = ColorScheme.lightenMask.night()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

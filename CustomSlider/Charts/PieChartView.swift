//
//  PieChartView.swift
//  CustomSlider
//
//  Created by Maxim on 16.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit
import QuartzCore

class PieChartView: ChartingView {
    
    private var xStackView: UIStackView!
    private var yStackView: UIStackView!
    private var pointsForPath = [[CGPoint]]()
    private var colorTheme: ColorTheme
    private var chartRect: CGRect {
        return CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height - Constants.xAxisLabelHeight - 2)
    }
    
    var pieLabels = [UILabel]()
    var xAxisLabels = [UILabel]()
    
    override init(frame: CGRect, colorTheme: ColorTheme, chartName: ChartName) {
        self.colorTheme = colorTheme
        super.init(frame: frame, colorTheme: colorTheme, chartName: chartName)
        lineColor = ColorScheme.axisTextSolidPie.day()
        labelTextColor = colorTheme == .day ? ColorScheme.axisTextSolidPie.day() : ColorScheme.axisTextSolidPie.night()
        
        layer.borderWidth = 2
        translatesAutoresizingMaskIntoConstraints = false
        contentMode = .redraw
        
//        let stack = drawCentralLabels()
//        stack.translatesAutoresizingMaskIntoConstraints = false
//        stack.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
//        stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
//        stack.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3).isActive = true
//        stack.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1/10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setDataForRequest(view: ChartingView, xmin: Int, xmax: Int, chartNum: Int) {
        chartRequest = ChartRequest(view: view, xmin: xmin, xmax: xmax, chartNum: chartNum)
        view.setNeedsLayout()
    }
    
    override func draw(_ rect: CGRect) {
        drawPieInRect(pointsForPath: pointsForPath, rect: chartRect)
    }
    
    override func layoutSubviews() {
        drawPie()
    }
    
    func drawPieInRect(pointsForPath: [[CGPoint]], rect: CGRect) {
        
        for row in pointsForPath.reversed().enumerated() {
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: row.element.first!)
            for point in row.element {
                path.addLine(to: point)
            }
            path.addLine(to: CGPoint(x: self.bounds.width, y: 0))
            path.close()
            path.lineWidth = Constants.chartLineWidthForChartView
            path.lineJoinStyle = .round
            colors.reversed()[row.offset].color.setFill()
            path.fill()
        }
    }
    
    func drawPie() {
        
        guard let request = chartRequest else { return }
        dataSource?.provideDataForPieChart(request: request) {
            chart in
            guard let percentageForPie = chart.percentageForPie else { return }
        
            colors = chart.colors
            if let ylt = chart.yLabelsText {
                yLabelText = ylt
            }
            
            var ratio: CGFloat {
                let r = CGFloat(request.xmax - request.xmin) / CGFloat(request.view.bounds.width)
                return r
            }
            
            if layer.sublayers != nil {
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.fade
                layer.add(transition, forKey: "transition")
            }
            
            if ratio > 4/11 {
                
                guard let sublayers = self.layer.sublayers else { return }
                for layer in sublayers {
                    layer.removeFromSuperlayer()
                }
                
                drawMarkLines(rect: chartRect)
                if let xlt = chart.xLabelsText {
                    layoutXaxisStackView(isHidden: false)
                    fillXLabelsWithText(textForLabels: xlt)
                }
                        
                        var shareArrays = [[CGFloat]]()
                        for dataRow in chart.ticks.enumerated() {
                            if let a = dataRow.element.shareArray {
                                shareArrays.append(a)
                            } else {
                                print("share array is empty")
                            }
                        }
                        
                        var newSharedArrays = [[CGFloat]]()
                        var tempArray = shareArrays[0]
                        newSharedArrays.append(tempArray)
                        var i = 1
                        repeat {
                            for sh in shareArrays[i].enumerated() {
                                tempArray[sh.offset] += sh.element
                            }
                            newSharedArrays.append(tempArray)
                            i += 1
                        } while (i != shareArrays.count)
                
                        var pointsForPath = [[CGPoint]]()
                        for dataRow in chart.ticks.enumerated() {
                            var points = [CGPoint]()
                            for (index, point) in dataRow.element.data.enumerated() {
                                let newPoint = CGPoint(x: point.x, y: newSharedArrays[dataRow.offset][index] * chartRect.size.height)
                                points.append(newPoint)
                            }
                            pointsForPath.append(points)
                        }
                        self.pointsForPath = pointsForPath
                        setNeedsDisplay()
                
                } else {
                
                layoutXaxisStackView(isHidden: true)
                
                guard let sublayers = self.layer.sublayers else { return }
                for layer in sublayers {
                    layer.removeFromSuperlayer()
                }
                
                let backLayer = CAShapeLayer()
                backLayer.backgroundColor = self.backgroundColor?.cgColor
                backLayer.frame = self.bounds
                self.layer.addSublayer(backLayer)
                
                let chartCenter = CGPoint(x: center.x, y: center.y)
                let shiftChartCenter = CGPoint(x: center.x + 2, y: center.y + 6)
                let radius = bounds.width/4
                let radiusShifted = radius + 2
                var centerPointForLabel = CGPoint()
                
                var i = 0
                var startAngle: CGFloat = 0
                var endAngle: CGFloat = 0
                
                repeat {
                    if i == 0 {
                        let piePiece = UIBezierPath()
                        endAngle = percentageForPie[i] * 2 * .pi
                        piePiece.move(to: shiftChartCenter)
                        piePiece.addLine(to: CGPoint(x: shiftChartCenter.x + radiusShifted, y: shiftChartCenter.y))
                        piePiece.addArc(withCenter: shiftChartCenter, radius: radiusShifted, startAngle: 0, endAngle: endAngle, clockwise: true)
                        piePiece.close()
                        
                        let shapeLayer = CAShapeLayer()
                        shapeLayer.path = piePiece.cgPath
                        shapeLayer.backgroundColor = UIColor.clear.cgColor
                        shapeLayer.fillColor = colors[i].color.cgColor
                        self.layer.addSublayer(shapeLayer)
                        startAngle = endAngle
                        centerPointForLabel = CGPoint(x: piePiece.bounds.midX, y: piePiece.bounds.midY)
                        createPieLabel(point: centerPointForLabel, text: "\(Int(percentageForPie[i] * 100))%")
                        layoutIfNeeded()
                    } else {
                        
                        let piePiece2 = UIBezierPath()
                        endAngle = endAngle + (percentageForPie[i] * 2 * .pi)
                        piePiece2.move(to: chartCenter)
                        piePiece2.addArc(withCenter: chartCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
                        piePiece2.addLine(to: chartCenter)
                        piePiece2.close()
                        
                        let shapeLayer = CAShapeLayer()
                        shapeLayer.path = piePiece2.cgPath
                        shapeLayer.backgroundColor = UIColor.clear.cgColor
                        shapeLayer.fillColor = colors[i].color.cgColor
                        self.layer.addSublayer(shapeLayer)
                        centerPointForLabel = calculatePosition(angle: startAngle + (percentageForPie[i] * 2 * .pi / 2), p: chartCenter, radius: radiusShifted * 2/3)
                        createPieLabel(point: centerPointForLabel, text: "\(Int(percentageForPie[i] * 100))%")
                        startAngle = endAngle
                        layoutIfNeeded()
                    }
                    i += 1
                } while (i != percentageForPie.count)
            }
        }
    }
    
    private func calculatePosition(angle: CGFloat, p: CGPoint, radius: CGFloat) -> CGPoint {
        return CGPoint(x: p.x + radius * cos(angle), y: p.y + radius * sin(angle))
    }
    
    private func createPieLabel(point: CGPoint, text: String) {
        let label = UILabel(frame: CGRect(origin: point, size: CGSize(width: 36, height: 18)))
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.text = text
        label.font = UIFont(name: "Helvetica-Bold", size: 16)
        label.sizeToFit()
        addSubview(label)
        label.center = point
        pieLabels.append(label)
    }
    
    private func fillXLabelsWithText(textForLabels: [String]) {
        
        xAxisLabels.first?.text = textForLabels.first
        xAxisLabels.last?.text = textForLabels.last
        
        let ratio = textForLabels.count / xAxisLabels.count
        for labelIndex in (1..<xAxisLabels.count - 1) {
            xAxisLabels[labelIndex].text = textForLabels[ratio * labelIndex]
        }
        for label in xAxisLabels {
            label.font = UIFont.boldSystemFont(ofSize: 12)
        }
    }

    private func configXlabels(markCount: Int) -> UIStackView {
        let axisWidth = self.bounds.width
        var axisLabels = [UILabel]()
        
        for _ in 0..<markCount {
            let label = UILabel(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: axisWidth / CGFloat(markCount), height: Constants.xAxisLabelHeight)))
            label.textColor = labelTextColor
            label.textAlignment = .center
            label.text = ""
            label.adjustsFontSizeToFitWidth = true
            axisLabels.append(label)
        }
        xAxisLabels = axisLabels
        
        let stackView = UIStackView(arrangedSubviews: xAxisLabels)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 1
        stackView.autoresizesSubviews = true
        
        self.addSubview(stackView)
        return stackView
    }
    
    private func layoutXaxisStackView(isHidden: Bool) {
        xStackView = configXlabels(markCount: Int(Int(self.bounds.width) / Int(Constants.xAxisLabelWidth)))
        xStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        xStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        xStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        xStackView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.1).isActive = false
        xStackView.isHidden = isHidden
    }
    
    private func drawMarkLines(rect: CGRect) {
        
        for i in 0..<Constants.yAxisLabelsCountForChart {
            let yPosition = rect.size.height - (rect.size.height) * CGFloat(i) / CGFloat(Constants.yAxisLabelsCountForChart)
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 30, y: yPosition))
            path.addLine(to: CGPoint(x: self.frame.maxX, y: yPosition))
            path.setLineDash([10, 2], count: 1, phase: 5)
            path.lineWidth = Constants.chartHorizontalLine
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = path.cgPath
            shapeLayer.backgroundColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = lineColor.cgColor
            self.layer.addSublayer(shapeLayer)

            placeYLabel(index: i, y: yPosition)
        }
    }
    
    private func placeYLabel(index: Int, y: CGFloat) {
        let yLabel = UILabel(frame: CGRect(origin: CGPoint(x: 12, y: y - Constants.heightForYLabel), size: CGSize(width: Constants.widthForYLabel, height: Constants.heightForYLabel)))
        yLabel.font = UIFont(name: "Arial", size: Constants.fontForYLabel)
        yLabel.textColor = labelTextColor
        yLabel.textAlignment = .right
        yLabel.text = yLabelText[index]
        yLabel.sizeToFit()
        self.addSubview(yLabel)
    }
    
    private func transferDataToLabels(intersection: CGPoint, color: UIColor, indexPath: Int, chartName: String) {
        
        let touchPoint = ypoints[indexPath].data.filter { Int($0.x) == Int(intersection.x) }
        
        if let fp = touchPoint.first, let interIndex = ypoints[indexPath].data.index(of: fp), let ldt = labelDateText {
            ldt("\(xLabelText[interIndex])")
            switch chartName {
            case "y0":
                labelChartOneText!("\(ypoints[indexPath].yArray[interIndex])")
                labelChartOne.textColor = color
            case "y1":
                labelChartTwoText!("\(ypoints[indexPath].yArray[interIndex])")
                labelChartTwo.textColor = color
            default:
                break
            }
        }
    }
    
    private func drawCentralLabels() -> UIStackView {
        var stack = UIStackView(frame: CGRect(x: self.bounds.width / 3, y: 10, width: self.bounds.width / 3, height: 40))
        labelDate = UILabel(frame: CGRect(x: stack.bounds.origin.x, y: stack.bounds.minY, width: stack.bounds.width * 2, height: 40))
        labelDate.numberOfLines = 2
        labelDate.textAlignment = .right
        
        labelChartOne = UILabel(frame: CGRect(x: stack.center.x, y: stack.bounds.minY, width: stack.bounds.width / 2, height: 20))
        labelChartOne.numberOfLines = 1
        
        labelChartTwo = UILabel(frame: CGRect(x: stack.center.x, y: stack.bounds.midY, width: stack.bounds.width / 2, height: 20))
        labelChartTwo.numberOfLines = 1
        
        switch colorTheme {
        case .day:
            labelDate.backgroundColor = ColorScheme.scrollBackground.day()
            labelChartOne.backgroundColor = ColorScheme.scrollBackground.day()
            labelChartTwo.backgroundColor = ColorScheme.scrollBackground.day()
        case .night:
            labelDate.backgroundColor = ColorScheme.scrollBackground.night()
            labelChartOne.backgroundColor = ColorScheme.scrollBackground.night()
            labelChartTwo.backgroundColor = ColorScheme.scrollBackground.night()
        }
        
        let vertStack = UIStackView(arrangedSubviews: [labelChartOne, labelChartTwo])
        vertStack.axis = .vertical
        vertStack.distribution = .fillEqually
        vertStack.spacing = 0
        
        stack = UIStackView(arrangedSubviews: [labelDate, vertStack])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 3
        self.addSubview(stack)
        return stack
    }
}




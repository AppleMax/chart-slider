//
//  ColorScheme.swift
//  CustomSlider
//
//  Created by Maxim on 18.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation
import UIKit

public enum ColorTheme {
    case day
    case night
}

enum ColorScheme {
    
    case scrollBackground
    case scrollSelector
    case gridLines
    case zoomOutText
    case toolTipArrow
    case lightenMask
    
    // line charts
    case redLineChart
    case redLineButton
    case redLineTooltipText
    case blueLineChart
    case blueLineButton
    case blueLineTooltipText
    case greenLineChart
    case greenLineButton
    case greenLineTooltipText
    case yellowLineChart
    case yellowLineButton
    case yellowLineTooltipText
    case axisTextChart
    
    // Online
    case onlineBlueLine
    case onlineDarkBlueLine
    case onlineLightBlueLine
    
    case onlineButtonBlueLine
    case onlineButtonDarkBlueLine
    case onlineButtonLightBlueLine
    
    case onlineTooltipBlueLine
    case onlineTooltipDarkBlueLine
    case onlineTooltipLightBlueLine
    
    // bar, solid, pie
    case barBlue
    case barDarkBlue
    case barLightGreen
    case barGreen
    case barYellow
    case barOrange
    case barRed
    case barLightBlue
    
    case solidBlue
    case solidDarkBlue
    case solidLightGreen
    case solidYellow
    case solidRed
    case solidLightBlue
    
    case solidButtonBlue
    case solidButtonDarkBlue
    case solidButtonLightGreen
    case solidButtonGreen
    case solidButtonYellow
    case solidButtonOrange
    case solidButtonRed
    case solidButtonLightBlue
    
    case solidTextBlue
    case solidTextDarkBlue
    case solidTextLightGreen
    case solidTextGreen
    case solidTextYellow
    case solidTextOrange
    case solidTextRed
    case solidTextLightBlue
    case solidAxisTextSolidPieX
    case solidAxisTextSolidPieY
    case axisTextSolidPie
    
    
    func night() -> UIColor {
        switch self {
        case .zoomOutText:
            return convertHexColorToUIColor(hexColor: "2EA6FE", alphaChannel: 1.0)
        case .gridLines:
            return convertHexColorToUIColor(hexColor: "8596AB", alphaChannel: 0.1)
        case .lightenMask:
            return convertHexColorToUIColor(hexColor: "212F3F", alphaChannel: 0.5) // darkenMask for night
        case .scrollSelector:
            return convertHexColorToUIColor(hexColor: "56626D", alphaChannel: 1.0)
        case .toolTipArrow:
            return convertHexColorToUIColor(hexColor: "D2D5D7", alphaChannel: 0.3)
        case .scrollBackground:
            return convertHexColorToUIColor(hexColor: "18222D", alphaChannel: 1.0)
            
            
        // followers, interactions, growth
        case .redLineChart:
            return convertHexColorToUIColor(hexColor: "FE3C30", alphaChannel: 1.0)
        case .redLineButton:
            return convertHexColorToUIColor(hexColor: "FF3B30", alphaChannel: 1.0)
        case .redLineTooltipText:
            return convertHexColorToUIColor(hexColor: "FE3C30", alphaChannel: 1.0)
        case .blueLineChart:
            return convertHexColorToUIColor(hexColor: "007AFF", alphaChannel: 1.0)
        case .blueLineButton:
            return convertHexColorToUIColor(hexColor: "3193FF", alphaChannel: 1.0)
        case .blueLineTooltipText:
            return convertHexColorToUIColor(hexColor: "3193FF", alphaChannel: 1.0)
        case .greenLineChart:
            return convertHexColorToUIColor(hexColor: "4BD964", alphaChannel: 1.0)
        case .greenLineButton:
            return convertHexColorToUIColor(hexColor: "4BD964", alphaChannel: 1.0)
        case .greenLineTooltipText:
            return convertHexColorToUIColor(hexColor: "4BD964", alphaChannel: 1.0)
        case .yellowLineChart:
            return convertHexColorToUIColor(hexColor: "F3BA00", alphaChannel: 1.0)
        case .yellowLineButton:
            return convertHexColorToUIColor(hexColor: "E0AC00", alphaChannel: 1.0)
        case .yellowLineTooltipText:
            return convertHexColorToUIColor(hexColor: "DDA900", alphaChannel: 1.0)
        case .axisTextChart:
            return convertHexColorToUIColor(hexColor: "8596AB", alphaChannel: 1.0)
            
        // bar, solid, pie
        case .barBlue:
            return convertHexColorToUIColor(hexColor: "407ECF", alphaChannel: 1.0)
        case .barDarkBlue:
            return convertHexColorToUIColor(hexColor: "2A61A9", alphaChannel: 1.0)
        case .barLightGreen:
            return convertHexColorToUIColor(hexColor: "88BA52", alphaChannel: 1.0)
        case .barGreen:
            return convertHexColorToUIColor(hexColor: "3DA05A", alphaChannel: 1.0)
        case .barYellow:
            return convertHexColorToUIColor(hexColor: "DBB630", alphaChannel: 1.0)
        case .barOrange:
            return convertHexColorToUIColor(hexColor: "E48C35", alphaChannel: 1.0)
        case .barRed:
            return convertHexColorToUIColor(hexColor: "DE4D43", alphaChannel: 1.0)
        case .barLightBlue:
            return convertHexColorToUIColor(hexColor: "479FC4", alphaChannel: 1.0)
            
        case .solidBlue:
            return convertHexColorToUIColor(hexColor: "407ECF", alphaChannel: 1.0)
        case .solidDarkBlue:
            return convertHexColorToUIColor(hexColor: "2A61A9", alphaChannel: 1.0)
        case .solidLightGreen:
            return convertHexColorToUIColor(hexColor: "88BA52", alphaChannel: 1.0)
        case .solidYellow:
            return convertHexColorToUIColor(hexColor: "DBB630", alphaChannel: 1.0)
        case .solidRed:
            return convertHexColorToUIColor(hexColor: "DE4D43", alphaChannel: 1.0)
        case .solidLightBlue:
            return convertHexColorToUIColor(hexColor: "479FC4", alphaChannel: 1.0)
        case .solidButtonBlue:
            return convertHexColorToUIColor(hexColor: "407ECF", alphaChannel: 1.0)
        case .solidButtonDarkBlue:
            return convertHexColorToUIColor(hexColor: "2A61A9", alphaChannel: 1.0)
        case .solidButtonLightGreen:
            return convertHexColorToUIColor(hexColor: "88BA52", alphaChannel: 1.0)
        case .solidButtonGreen:
            return convertHexColorToUIColor(hexColor: "3DA05A", alphaChannel: 1.0)
        case .solidButtonYellow:
            return convertHexColorToUIColor(hexColor: "DBB630", alphaChannel: 1.0)
        case .solidButtonOrange:
            return convertHexColorToUIColor(hexColor: "E48C35", alphaChannel: 1.0)
        case .solidButtonRed:
            return convertHexColorToUIColor(hexColor: "DE4D43", alphaChannel: 1.0)
        case .solidButtonLightBlue:
            return convertHexColorToUIColor(hexColor: "399FC5", alphaChannel: 1.0)
        case .solidTextBlue:
            return convertHexColorToUIColor(hexColor: "63A6FF", alphaChannel: 1.0)
        case .solidTextDarkBlue:
            return convertHexColorToUIColor(hexColor: "3578CF", alphaChannel: 1.0)
        case .solidTextLightGreen:
            return convertHexColorToUIColor(hexColor: "86C740", alphaChannel: 1.0)
        case .solidTextGreen:
            return convertHexColorToUIColor(hexColor: "3DA05A", alphaChannel: 1.0)
        case .solidTextYellow:
            return convertHexColorToUIColor(hexColor: "DBB630", alphaChannel: 1.0)
        case .solidTextOrange:
            return convertHexColorToUIColor(hexColor: "FF9B39", alphaChannel: 1.0)
        case .solidTextRed:
            return convertHexColorToUIColor(hexColor: "DE4D43", alphaChannel: 1.0)
        case .solidTextLightBlue:
            return convertHexColorToUIColor(hexColor: "269ED4", alphaChannel: 1.0)
        case .solidAxisTextSolidPieX:
            return convertHexColorToUIColor(hexColor: "8596AB", alphaChannel: 1.0)
        case .solidAxisTextSolidPieY:
            return convertHexColorToUIColor(hexColor: "BACCE1", alphaChannel: 0.6)
            
        case .onlineBlueLine:
            return convertHexColorToUIColor(hexColor: "3381e8", alphaChannel: 1.0)
        case .onlineDarkBlueLine:
            return convertHexColorToUIColor(hexColor: "4461ab", alphaChannel: 1.0)
        case .onlineLightBlueLine:
            return convertHexColorToUIColor(hexColor: "4697B3", alphaChannel: 1.0)
        case .onlineButtonBlueLine:
            return convertHexColorToUIColor(hexColor: "539af7", alphaChannel: 1.0)
        case .onlineButtonDarkBlueLine:
            return convertHexColorToUIColor(hexColor: "4461ab", alphaChannel: 1.0)
        case .onlineButtonLightBlueLine:
            return convertHexColorToUIColor(hexColor: "4697B3", alphaChannel: 1.0)
        case .onlineTooltipBlueLine:
            return convertHexColorToUIColor(hexColor: "3381e8", alphaChannel: 1.0)
        case .onlineTooltipDarkBlueLine:
            return convertHexColorToUIColor(hexColor: "4461ab", alphaChannel: 1.0)
        case .onlineTooltipLightBlueLine:
            return convertHexColorToUIColor(hexColor: "4697B3", alphaChannel: 1.0)
            
        case .axisTextSolidPie:
            return convertHexColorToUIColor(hexColor: "BACCE1", alphaChannel: 0.6)
        }
    }
    
    func day() -> UIColor {
        switch self {
        
        case .zoomOutText:
            return convertHexColorToUIColor(hexColor: "108BE3", alphaChannel: 1.0)
        case .gridLines:
            return convertHexColorToUIColor(hexColor: "182D3B", alphaChannel: 0.1)
        case .lightenMask:
            return convertHexColorToUIColor(hexColor: "FFFFFF", alphaChannel: 0.5)
        case .scrollSelector:
            return convertHexColorToUIColor(hexColor: "C0D1E1", alphaChannel: 1.0)
        case .toolTipArrow:
            return convertHexColorToUIColor(hexColor: "59606D", alphaChannel: 0.3)
        case .scrollBackground:
            return convertHexColorToUIColor(hexColor: "E2EEF9", alphaChannel: 1.0)
            
          // followers, interactions, growth
        case .redLineChart:
            return convertHexColorToUIColor(hexColor: "FE3C30", alphaChannel: 1.0)
        case .redLineButton:
            return convertHexColorToUIColor(hexColor: "FF3B30", alphaChannel: 1.0)
        case .redLineTooltipText:
            return convertHexColorToUIColor(hexColor: "FE3C30", alphaChannel: 1.0)
        case .blueLineChart:
            return convertHexColorToUIColor(hexColor: "007AFF", alphaChannel: 1.0)
        case .blueLineButton:
            return convertHexColorToUIColor(hexColor: "007AFF", alphaChannel: 1.0)
        case .blueLineTooltipText:
            return convertHexColorToUIColor(hexColor: "007AFF", alphaChannel: 1.0)
        case .greenLineChart:
            return convertHexColorToUIColor(hexColor: "4BD964", alphaChannel: 1.0)
        case .greenLineButton:
            return convertHexColorToUIColor(hexColor: "4BD964", alphaChannel: 1.0)
        case .greenLineTooltipText:
            return convertHexColorToUIColor(hexColor: "2FB146", alphaChannel: 1.0)
        case .yellowLineChart:
            return convertHexColorToUIColor(hexColor: "F3BA00", alphaChannel: 1.0)
        case .yellowLineButton:
            return convertHexColorToUIColor(hexColor: "F3BA00", alphaChannel: 1.0)
        case .yellowLineTooltipText:
            return convertHexColorToUIColor(hexColor: "DDA900", alphaChannel: 1.0)
        case .axisTextChart:
            return convertHexColorToUIColor(hexColor: "8E8E93", alphaChannel: 1.0)
        
            // bar, solid, pie
        case .barBlue:
            return convertHexColorToUIColor(hexColor: "539af7", alphaChannel: 1.0)
        case .barDarkBlue:
            return convertHexColorToUIColor(hexColor: "407dcd", alphaChannel: 1.0)
        case .barLightGreen:
            return convertHexColorToUIColor(hexColor: "88d43f", alphaChannel: 1.0)
        case .barGreen:
            return convertHexColorToUIColor(hexColor: "73b435", alphaChannel: 1.0)
        case .barYellow:
            return convertHexColorToUIColor(hexColor: "f5bd25", alphaChannel: 1.0)
        case .barOrange:
            return convertHexColorToUIColor(hexColor: "ff9b39", alphaChannel: 1.0)
        case .barRed:
            return convertHexColorToUIColor(hexColor: "f8564b", alphaChannel: 1.0)
        case .barLightBlue:
            return convertHexColorToUIColor(hexColor: "539af7", alphaChannel: 1.0)
            
        case .solidBlue:
            return convertHexColorToUIColor(hexColor: "3497ed", alphaChannel: 1.0)
        case .solidDarkBlue:
            return convertHexColorToUIColor(hexColor: "2373db", alphaChannel: 1.0)
        case .solidLightGreen:
            return convertHexColorToUIColor(hexColor: "88d43f", alphaChannel: 1.0)
        case .solidYellow:
            return convertHexColorToUIColor(hexColor: "ffce2c", alphaChannel: 1.0)
        case .solidRed:
            return convertHexColorToUIColor(hexColor: "e65850", alphaChannel: 1.0)
        case .solidLightBlue:
            return convertHexColorToUIColor(hexColor: "55bfe6", alphaChannel: 1.0)
        case .solidButtonBlue:
            return convertHexColorToUIColor(hexColor: "539af7", alphaChannel: 1.0)
        case .solidButtonDarkBlue:
            return convertHexColorToUIColor(hexColor: "407DCD", alphaChannel: 1.0)
        case .solidButtonLightGreen:
            return convertHexColorToUIColor(hexColor: "88D43F", alphaChannel: 1.0)
        case .solidButtonGreen:
            return convertHexColorToUIColor(hexColor: "73B435", alphaChannel: 1.0)
        case .solidButtonYellow:
            return convertHexColorToUIColor(hexColor: "F5BD25", alphaChannel: 1.0)
        case .solidButtonOrange:
            return convertHexColorToUIColor(hexColor: "FF9B39", alphaChannel: 1.0)
        case .solidButtonRed:
            return convertHexColorToUIColor(hexColor: "F8564B", alphaChannel: 1.0)
        case .solidButtonLightBlue:
            return convertHexColorToUIColor(hexColor: "55BFE6", alphaChannel: 1.0)
        case .solidTextBlue:
            return convertHexColorToUIColor(hexColor: "539AF7", alphaChannel: 1.0)
        case .solidTextDarkBlue:
            return convertHexColorToUIColor(hexColor: "407DCD", alphaChannel: 1.0)
        case .solidTextLightGreen:
            return convertHexColorToUIColor(hexColor: "73C129", alphaChannel: 1.0)
        case .solidTextGreen:
            return convertHexColorToUIColor(hexColor: "4BAB29", alphaChannel: 1.0)
        case .solidTextYellow:
            return convertHexColorToUIColor(hexColor: "EAAF10", alphaChannel: 1.0)
        case .solidTextOrange:
            return convertHexColorToUIColor(hexColor: "E87B11", alphaChannel: 1.0)
        case .solidTextRed:
            return convertHexColorToUIColor(hexColor: "F34C44", alphaChannel: 1.0)
        case .solidTextLightBlue:
            return convertHexColorToUIColor(hexColor: "269ED4", alphaChannel: 1.0)
        case .axisTextSolidPie:
            return convertHexColorToUIColor(hexColor: "252529", alphaChannel: 0.5)

        case .onlineBlueLine:
            return convertHexColorToUIColor(hexColor: "77BAF4", alphaChannel: 1.0)
        case .onlineDarkBlueLine:
            return convertHexColorToUIColor(hexColor: "3381E8", alphaChannel: 1.0)
        case .onlineLightBlueLine:
            return convertHexColorToUIColor(hexColor: "74DAFF", alphaChannel: 1.0)
        case .onlineButtonBlueLine:
            return convertHexColorToUIColor(hexColor: "77BAF4", alphaChannel: 1.0)
        case .onlineButtonDarkBlueLine:
            return convertHexColorToUIColor(hexColor: "539AF7", alphaChannel: 1.0)
        case .onlineButtonLightBlueLine:
            return convertHexColorToUIColor(hexColor: "74DAFF", alphaChannel: 1.0)
        case .onlineTooltipBlueLine:
            return convertHexColorToUIColor(hexColor: "67B2F2", alphaChannel: 1.0)
        case .onlineTooltipDarkBlueLine:
            return convertHexColorToUIColor(hexColor: "3381E8", alphaChannel: 1.0)
        case .onlineTooltipLightBlueLine:
            return convertHexColorToUIColor(hexColor: "55BFE6", alphaChannel: 1.0)
        
        case .solidAxisTextSolidPieX:
            return convertHexColorToUIColor(hexColor: "252529", alphaChannel: 0.5)
        case .solidAxisTextSolidPieY:
            return convertHexColorToUIColor(hexColor: "252529", alphaChannel: 0.5)
        }
    }
    
    func convertHexColorToUIColor(hexColor: String, alphaChannel: CGFloat) -> UIColor {
        var hexString: String = hexColor.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (hexString.hasPrefix("#")) {
            hexString.remove(at: hexString.startIndex)
        }
        
        if ((hexString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue: UInt32 = 0
        _ = Scanner(string: hexString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alphaChannel
        )
    }
}

extension UITableViewCell {
    open func setColorTheme(colorTheme: ColorTheme) {
        switch colorTheme {
        case .day:
            backgroundColor = ColorScheme.scrollBackground.day()
            textLabel?.textColor = ColorScheme.axisTextChart.day()
        case .night:
            backgroundColor = ColorScheme.scrollBackground.night()
            textLabel?.textColor = ColorScheme.axisTextChart.night()
        }
    }
}

extension UITableView {
    open func setColorTheme(colorTheme: ColorTheme) {
        switch colorTheme {
        case .day:
            backgroundColor = ColorScheme.scrollBackground.day()
            separatorColor = ColorScheme.scrollBackground.day()
            self.reloadData()
        case .night:
            backgroundColor = ColorScheme.scrollBackground.night()
            separatorColor = ColorScheme.scrollBackground.night()
            self.reloadData()
        }
    }
}



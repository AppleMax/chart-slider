//
//  BarChartView.swift
//  CustomSlider
//
//  Created by Maxim on 16.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit

class BarChartView: ChartingView {
    
    private var xStackView: UIStackView!
    private var yStackView: UIStackView!
    private var tapper: UITapGestureRecognizer!
    var touchPoint = CGPoint.zero {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    private var colorTheme: ColorTheme
    
    override init(frame: CGRect, colorTheme: ColorTheme, chartName: ChartName) {
        self.colorTheme = colorTheme
        super.init(frame: frame, colorTheme: colorTheme, chartName: chartName)
        
        layer.borderWidth = 1
        translatesAutoresizingMaskIntoConstraints = false
        
        xStackView = configXlabels(markCount: Int(Int(self.bounds.width) / 80))
        xStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        xStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        xStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        xStackView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.1)
        
        let stack = drawCentralLabels()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        stack.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3).isActive = true
        stack.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1/10).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        drawChart()
        //drawLinePath()
        drawMarkLines()
    }
    
    override func drawChart() {
        //super.drawChart()
        guard let request = chartRequest else { return }
        let context = UIGraphicsGetCurrentContext()
        let viewWidth = request.view.bounds.width
        let viewHeight = request.view.bounds.height
        let barQuantity: Int = Int(viewWidth / Constants.barWidth)
        dataSource?.provideDataForBarChart(request: request) {
            chart in
            colors = chart.colors
            if let textY = chart.yLabelsText {
                yLabelText = textY
            }
            if let textX = chart.xLabelsText {
                xLabelText = textX
            }
            
            if chart.stacked != nil {
            var rectOrigin = CGPoint(x: 0, y: viewHeight/2)
            var clipOrigin = CGPoint(x: 0, y: viewHeight/1.5)
            
            for _ in 0..<barQuantity {
                let barRect = CGRect(origin: rectOrigin, size: CGSize(width: Constants.barWidth, height: self.bounds.height - rectOrigin.y))
                let bar = UIBezierPath(rect: barRect)
                
                bar.lineWidth = 0
                colors[3].color.setFill()
                bar.fill()
                
                context?.saveGState()
                let clipHeight = Int.random(in: 20...100)
                let clipRect = CGRect(origin: rectOrigin, size: CGSize(width: Constants.barWidth, height: CGFloat(clipHeight)))
                let clippedArea = UIBezierPath(rect: clipRect)
                clippedArea.addClip()
                colors[5].color.setFill()
                clippedArea.fill()
                context?.restoreGState()
                
                rectOrigin.x += Constants.barWidth
                rectOrigin.y = CGFloat(Int.random(in: Int((viewHeight) / 5)...Int(viewHeight) / 2))
                clipOrigin.x += Constants.barWidth
                clipOrigin.y = CGFloat(Int.random(in: 300...400))
                }
                    
            } else {
                var rectOrigin = CGPoint(x: 0, y: viewHeight/2)
                for _ in 0..<barQuantity {
                let barRect = CGRect(origin: rectOrigin, size: CGSize(width: Constants.barWidth, height: self.bounds.height - rectOrigin.y))
                let bar = UIBezierPath(rect: barRect)
                colors[0].color.setFill()
                    bar.fill()
                    rectOrigin.x += Constants.barWidth
                    rectOrigin.y = CGFloat(Int.random(in: Int((viewHeight) / 5)...Int(viewHeight) / 2))
            }
        }
        }
        
//        guard let xl = xlabels, let xlt = xLabelText else { return }
//        xl.first?.text = xlt.first
//        xl.last?.text = xlt.last
//
//        let ratio = xlt.count / xl.count
//        for labelIndex in (1..<xl.count - 1) {
//            xl[labelIndex].text = xlt[ratio * labelIndex]
//        }
//        for label in xl {
//            label.font = UIFont.boldSystemFont(ofSize: 12)
//        }
        
        
    }
    
    private func configXlabels(markCount: Int) -> UIStackView {
        let axisWidth = self.bounds.width
        xlabels = [UILabel]()
        
        for _ in 0..<markCount {
            let label = UILabel(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: axisWidth / CGFloat(markCount), height: 12)))
            label.adjustsFontSizeToFitWidth = true
            label.textColor = labelTextColor
            label.textAlignment = .center
            xlabels.append(label)
        }
        let stackView = UIStackView(arrangedSubviews: xlabels)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 1
        stackView.autoresizesSubviews = true
        self.addSubview(stackView)
        
        return stackView
    }
    
    private func drawMarkLines() {
        
        for i in 0..<Constants.yAxisLabelsCountForChart {
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 30, y: heightForChart - heightForChart * CGFloat(i) / CGFloat(Constants.yAxisLabelsCountForChart) - 12))
            path.addLine(to: CGPoint(x: self.frame.maxX, y: heightForChart - heightForChart * CGFloat(i) / CGFloat(Constants.yAxisLabelsCountForChart) - 12))
            path.setLineDash([10, 2], count: 1, phase: 5)
            path.lineWidth = Constants.chartHorizontalLine
            lineColor.setStroke()
            path.stroke()
            placeYLabel(index: i, y: heightForChart * CGFloat(i + 1) / CGFloat(Constants.yAxisLabelsCountForChart))
        }
    }
    
    private func placeYLabel(index: Int, y: CGFloat) {
        let yLabel = UILabel(frame: CGRect(origin: CGPoint(x: 12, y: y - Constants.heightForYLabel * 1.5), size: CGSize(width: Constants.widthForYLabel, height: Constants.heightForYLabel)))
        yLabel.font = UIFont(name: "Arial", size: Constants.fontForYLabel)
        yLabel.textColor = labelTextColor
        yLabel.textAlignment = .right
        yLabel.text = yLabelText[index]
        yLabel.sizeToFit()
        self.addSubview(yLabel)
    }
    
    private func drawLinePath() {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: Int(touchPoint.x), y: 0))
        path.addLine(to: CGPoint(x: Int(touchPoint.x), y: Int(self.frame.maxY)))
        path.close()
        
        path.lineWidth = Constants.chartVerticalLine
        lineColor.setStroke()
        findIntersection(line: path)
        path.stroke()
    }
    
    /**
     point.element.x +/- is a sensitivity threshold for finding an intersection btw 2 paths
     - Parameter line: a vertical indicator line on the chart
 */
    
    private func findIntersection(line: UIBezierPath) {
        
        guard let hc = heightCoefficient else { return }
        for row in ypoints.enumerated() {
            var circleColor: UIColor
            var backColor: UIColor
            if !ChartingView.joinedChannelIsOn && row.offset == 0 {
                circleColor = UIColor.clear
                backColor = UIColor.clear
            } else {
                circleColor = colors[row.offset].color
                backColor = self.backgroundColor ?? ColorScheme.scrollBackground.night()
            }
        for point in ypoints[row.offset].data.enumerated() {
                for touchX in Int(point.element.x)...Int(point.element.x) where Int(point.element.x) != 0 {
                    let intPoint = CGPoint(x: touchX, y: Int(heightForChart - point.element.y * hc))
                if line.contains(intPoint) {
                    drawLabelAtPoint(intersection: intPoint, color: circleColor, backColor: backColor)
                    transferDataToLabels(intersection: intPoint, color: circleColor, indexPath: row.offset, chartName: row.element.name)
                    }
                }
            }
        }
    }
    
    private func drawLabelAtPoint(intersection: CGPoint, color: UIColor, backColor: UIColor) {
        let label = UILabel(frame: CGRect(x: intersection.x - 6, y: intersection.y - 8, width: 12, height: 12))
        self.addSubview(label)
        label.backgroundColor = backColor
        label.clipsToBounds = true
        label.layer.borderColor = color.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = 6
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            label.removeFromSuperview()
        }
    }
    
    private func transferDataToLabels(intersection: CGPoint, color: UIColor, indexPath: Int, chartName: String) {
        
        let touchPoint = ypoints[indexPath].data.filter { Int($0.x) == Int(intersection.x) }
        
        if let fp = touchPoint.first, let interIndex = ypoints[indexPath].data.index(of: fp), let ldt = labelDateText {
            ldt("\(xLabelText[interIndex])")
            switch chartName {
            case "y0":
                labelChartOneText!("\(ypoints[indexPath].yArray[interIndex])")
                labelChartOne.textColor = color
            case "y1":
                labelChartTwoText!("\(ypoints[indexPath].yArray[interIndex])")
                labelChartTwo.textColor = color
            default:
                break
            }
        }
    }
    
    private func drawCentralLabels() -> UIStackView {
        var stack = UIStackView(frame: CGRect(x: self.bounds.width / 3, y: 10, width: self.bounds.width / 3, height: 40))
        labelDate = UILabel(frame: CGRect(x: stack.bounds.origin.x, y: stack.bounds.minY, width: stack.bounds.width * 2, height: 40))
        labelDate.numberOfLines = 2
        labelDate.textAlignment = .right
        
        labelChartOne = UILabel(frame: CGRect(x: stack.center.x, y: stack.bounds.minY, width: stack.bounds.width / 2, height: 20))
        labelChartOne.numberOfLines = 1
        
        labelChartTwo = UILabel(frame: CGRect(x: stack.center.x, y: stack.bounds.midY, width: stack.bounds.width / 2, height: 20))
        labelChartTwo.numberOfLines = 1
        
        switch colorTheme {
        case .day:
            labelDate.backgroundColor = ColorScheme.scrollBackground.day()
            labelChartOne.backgroundColor = ColorScheme.scrollBackground.day()
            labelChartTwo.backgroundColor = ColorScheme.scrollBackground.day()
        case .night:
            labelDate.backgroundColor = ColorScheme.scrollBackground.night()
            labelChartOne.backgroundColor = ColorScheme.scrollBackground.night()
            labelChartTwo.backgroundColor = ColorScheme.scrollBackground.night()
        }
        
        let vertStack = UIStackView(arrangedSubviews: [labelChartOne, labelChartTwo])
        vertStack.axis = .vertical
        vertStack.distribution = .fillEqually
        vertStack.spacing = 0
        
        stack = UIStackView(arrangedSubviews: [labelDate, vertStack])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 3
        self.addSubview(stack)
        return stack
    }
}




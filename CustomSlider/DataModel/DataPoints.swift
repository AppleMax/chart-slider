//
//  DataPoints.swift
//  CustomSlider
//
//  Created by Maxim on 16.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation
import UIKit

/**
 DataSource for `ChartingView`, caller provides a `ChartRequest`, receives back a `Chart` object
 - `calculationQueue`: added to ensure there is no main thread overload
 - `calculationQueue.waitUntilAllOperationsAreFinished()` is used in auxiliary methods to guarantee the job is done before a method returns, otherwise prone to errors
 */

class DataPoints {
    
    private let converter: DataConverter?
    private let calculationQueue = OperationQueue()
    private var ticksForLabels: [AxisData]?
    private var rootFolderName = ""
    
    private var monthChartArray: [RawChart]?
    var month: String = "" {
        didSet {
            do {
                monthChartArray = try convertMonthlyDataToChartData(jsonData: monthlyData, monthName: month)
            } catch {
                print("ERROR MAKING MONTH ARRAY: \(error.localizedDescription)")
            }
        }
    }
    
    private lazy var rawcharts: [RawChart] = {
        guard let r = convertRawDataFromJson() else { return [RawChart]() }
        return r
    }()
    private lazy var monthlyData: [MaxTlg] = {
        guard let c = converter else { return [MaxTlg]() }
        return c.fetchMonthData(folderName: rootFolderName)
    }()
    
    
    init(chartName: ChartName) {
        
        rootFolderName = chartName.fetchMonthFoldersPath()
        converter = DataConverter(chartFolder: rootFolderName)
        calculationQueue.qualityOfService = .userInteractive
        calculationQueue.name = "Calculation Queue"
        
    }
    
    private func convertRawDataFromJson() -> [RawChart]? {
        
        var rcharts = [RawChart]()
        guard let chartGroup = converter?.charts else { return nil }
        calculationQueue.addOperation {

            var rawchart = RawChart(data: chartGroup.columns, name: chartGroup.names, color: chartGroup.colors, type: chartGroup.types, id: 0, stacked: nil, y_scaled: nil, percentage: nil)
            
            if let st = chartGroup.stacked {
                print("Stacked: \(st)")
                rawchart.stacked = st
            }
            if let y_sc = chartGroup.y_scaled {
                print("Y-Scaled: \(y_sc)")
                rawchart.y_scaled = y_sc
            }
            if let perc = chartGroup.percentage {
                print("Percentage: \(perc)")
                rawchart.percentage = perc
            }
            
             rcharts.append(rawchart)
        }
        calculationQueue.waitUntilAllOperationsAreFinished()
        return rcharts
    }
    
    private func convertMonthlyDataToChartData(jsonData: [MaxTlg], monthName: String) throws -> [RawChart] {
        var rcharts = [RawChart]()
        calculationQueue.addOperation {

        let monthD = jsonData.filter { $0.month == monthName }.flatMap { $0.monthData }
        monthD.forEach { day in
            for (index, group) in monthD.enumerated() {
                let rawchart = RawChart(data: group.columns, name: group.names, color: group.colors, type: group.types, id: index, stacked: group.stacked, y_scaled: group.y_scaled, percentage: group.percentage)
                rcharts.append(rawchart)
            }
        }
        }
        calculationQueue.waitUntilAllOperationsAreFinished()
        return rcharts
    }
    
    private func makeChart(chart number: Int) -> Chart? {
        if number < rawcharts.count {
            let names = rawcharts[number].name
            let types = rawcharts[number].type
            let colors = makeChartColors(rawChart: rawcharts[number])
            
            let ticks = makeTicksForChart(rawCharts: rawcharts, number: number)
            ticksForLabels = ticks
            let chart = Chart(name: names, type: types, colors: colors, ticks: ticks, stacked: rawcharts[number].stacked, y_scaled: rawcharts[number].y_scaled, percentage: rawcharts[number].percentage)
            return chart
        } else {
            fatalError("CHART NUMBER: \(number) DOES NOT EXIST")
        }
    }
    
    private func makeChartColors(rawChart: RawChart) -> [ChartColor] {
        var chartColors = [ChartColor]()
        
        let y0c = convertHexColorToUIColor(hexColor: rawChart.color.y0, alphaChannel: 1)
        let cc0 = ChartColor(name: rawChart.name.y0, color: y0c)
        chartColors.append(cc0)
        
        guard let y1 = rawChart.color.y1, let n1 = rawChart.name.y1 else {
            return chartColors }
        let y1c = convertHexColorToUIColor(hexColor: y1, alphaChannel: 1)
        let cc1 = ChartColor(name: n1, color: y1c)
        chartColors.append(cc1)
    
        guard let y2 = rawChart.color.y2, let n2 = rawChart.name.y2 else {
            return chartColors
        }
        let y2c = convertHexColorToUIColor(hexColor: y2, alphaChannel: 1)
        let cc2 = ChartColor(name: n2, color: y2c)
        chartColors.append(cc2)
        
        guard let y3 = rawChart.color.y3, let n3 = rawChart.name.y3 else {
            return chartColors
        }
        let y3c = convertHexColorToUIColor(hexColor: y3, alphaChannel: 1)
        let cc3 = ChartColor(name: n3, color: y3c)
        chartColors.append(cc3)
        
        guard let y4 = rawChart.color.y4, let n4 = rawChart.name.y4 else {
            return chartColors
            
        }
        let y4c = convertHexColorToUIColor(hexColor: y4, alphaChannel: 1)
        let cc4 = ChartColor(name: n4, color: y4c)
        chartColors.append(cc4)
        
        guard let y5 = rawChart.color.y5, let n5 = rawChart.name.y5 else {
            return chartColors
        }
        let y5c = convertHexColorToUIColor(hexColor: y5, alphaChannel: 1)
        let cc5 = ChartColor(name: n5, color: y5c)
        chartColors.append(cc5)
        
        guard let y6 = rawChart.color.y6, let n6 = rawChart.name.y6 else {
            return chartColors
        }
        let y6c = convertHexColorToUIColor(hexColor: y6, alphaChannel: 1)
        let cc6 = ChartColor(name: n6, color: y6c)
        chartColors.append(cc6)
        return chartColors
    }
    
    private func makeTicksForChart(rawCharts: [RawChart], number: Int) -> [AxisData] {
        
        var arr = [AxisData]()
        let rawChart = rawCharts[number]
        arr = makeAxisArray(rawChart: rawChart)
        return arr
    }
    
    private func makeAxisArray(rawChart: RawChart) -> [AxisData] {
        var axisArray = [AxisData]()
        
        calculationQueue.addOperation {
            for j in 1..<rawChart.dataColumns.count {
                let column = rawChart.dataColumns[j]
                var pointArray = [CGPoint]()
                var yArray = [Int]()
                var axisMark = ""
                for (k, value) in column.enumerated() {
                    switch value {
                    case .integer(let y):
                        let point = CGPoint(x: k, y: y)
                        yArray.append(y)
                        pointArray.append(point)
                    case .string(let mark):
                        axisMark = mark
                    }
                }
                let a = AxisData(name: axisMark, yArray: yArray, data: pointArray)
                axisArray.append(a)
            }
        }
        calculationQueue.waitUntilAllOperationsAreFinished()
        return axisArray
    }
    
    private func makeAxisForChart(chart: Chart, request: ChartRequest) -> [AxisData] {
        var axisDataArray = [AxisData]()
        //let viewWidth = request.view.frame.size.width
        
        calculationQueue.addOperation {
            for (index, axisdata) in chart.ticks.enumerated() {
                var chartPoints = [CGPoint]()
                //let widthCoef: CGFloat = viewWidth / CGFloat(chart.ticks[index].data.count)
                
                for point in axisdata.data.enumerated() {
                    let newX = CGFloat(point.offset)
                        //* widthCoef
                    let newY: CGFloat = point.element.y
                    chartPoints.append(CGPoint(x: newX, y: newY))
                }
                let aD = AxisData(name: chart.ticks[index].name, yArray: chart.ticks[index].yArray, data: chartPoints)
                axisDataArray.append(aD)
            }
        }
        calculationQueue.waitUntilAllOperationsAreFinished()
        return axisDataArray
    }
    
    /**
     A method which returns a Chart object to a view specified in ChartRequest
     - Parameter request: ChartRequest passing the calling view of ChartingView type
     - Parameter completion: ready-to-go Chart object with `[CGPoint]` for drawing
     */
    
    func makeChartDataFor(request: ChartRequest, completion: (Chart) -> ()){
        
        guard let chart = makeChart(chart: request.chartNumber) else { return }
        let requestViewWidth = request.view.bounds.width
        let requestViewHeight = request.view.bounds.height
        let requestMinX = CGFloat(request.xmin)
        let requestMaxX = CGFloat(request.xmax)
        let axisDataArray = makeAxisForChart(chart: chart, request: request)
        var maximumYarray = [CGFloat]()
        
        var finalAxisData = [AxisData]()
        //var finalXLabels = [String]()
        var heightCoefficientForChart: CGFloat = 1.0
        
        calculationQueue.addOperation {
            
            for (index, array) in axisDataArray.enumerated() {
                let maximumY = chart.ticks[index].data.max{$1.y > $0.y}!.y
                maximumYarray.append(maximumY)
                
                let pointCount: CGFloat = CGFloat(chart.ticks[index].data.count)
                let startIndex = Int((requestMinX/requestViewWidth) * pointCount)
                let endIndex = Int((requestMaxX/requestViewWidth) * pointCount)
                let safeStartIndex = startIndex >= 0 ? startIndex : 0
                let safeEndIndex = Int(endIndex) < Int(pointCount) ? Int(endIndex) : Int(pointCount)
                
                let selectedChartPoints = array.data[safeStartIndex..<safeEndIndex]
                let selectedYvalues = Array(array.yArray[safeStartIndex..<safeEndIndex])
                
                var finalPoints = [CGPoint]()
                let localWidthCoef: CGFloat = requestViewWidth / CGFloat(selectedChartPoints.count)
                
                for selectedPoint in selectedChartPoints.enumerated() {
                        let t1 = CGPoint(x: CGFloat(selectedPoint.offset) * localWidthCoef, y: selectedPoint.element.y )
                        finalPoints.append(t1)
                    }
                
                let axData = AxisData(name: chart.ticks[index].name, yArray: selectedYvalues, data: finalPoints)
                finalAxisData.append(axData)
            }
             heightCoefficientForChart = requestViewHeight * 0.8 / maximumYarray.max()!
        }
        
//            let labels = self.makeMarksForXLabels(chartNumber: request.chartNumber)
//
//            var startIndex = Int(requestMinX/requestViewWidth * CGFloat(labels.count))
//            var endIndex = Int(requestMaxX/requestViewWidth * CGFloat(labels.count))
//
//            guard startIndex >= 0 else { startIndex = 0; return }
//            guard endIndex <= labels.count else { endIndex = labels.count; return }
//            let selectedLabels = Array(labels[startIndex..<endIndex])
//            for label in selectedLabels {
//                finalXLabels.append(label)
//            }
        
        
        calculationQueue.waitUntilAllOperationsAreFinished()
        var newChart = Chart(name: chart.name, type: chart.type, colors: chart.colors, ticks: finalAxisData, stacked: chart.stacked, y_scaled: chart.y_scaled, percentage: chart.percentage)
        newChart.yLabelsText = makeMarksForYLabels(chartNumber: request.chartNumber)
        newChart.xLabelsText = createXLabelsDynamically(chartNumber: request.chartNumber, minX: requestMinX, maxX: requestMaxX, viewWidth: requestViewWidth)
        newChart.heightCoefForChart = heightCoefficientForChart
        completion(newChart)
    }
    
    func createXLabelsDynamically(chartNumber: Int, minX: CGFloat, maxX: CGFloat, viewWidth: CGFloat) -> [String] {
        var finalXLabels = [String]()
        let labels = self.makeMarksForXLabels(chartNumber: chartNumber)
        
        var startIndex = Int(minX/viewWidth * CGFloat(labels.count))
        var endIndex = Int(maxX/viewWidth * CGFloat(labels.count))
        
        guard startIndex >= 0 else { startIndex = 0; return [" "] }
        guard endIndex <= labels.count else { endIndex = labels.count; return [" "] }
        let selectedLabels = Array(labels[startIndex..<endIndex])
        for label in selectedLabels {
            finalXLabels.append(label)
        }
        
        return finalXLabels
    }
    
    func makeChartDataForPie(request: ChartRequest, completion: (Chart) -> ()) {
        guard let chart = makeChart(chart: request.chartNumber) else { return }
        var pieData = [AxisData]()
        var summarizedArray = chart.ticks[0].yArray
        for row in chart.ticks.enumerated() {
            if row.offset == 0 {
                continue
            }
            for i in 0..<row.element.yArray.count {
                summarizedArray[i] += row.element.yArray[i]
            }
        }
        
        //let widthCoefficient = request.view.bounds.width / CGFloat(summarizedArray.count)
        let heightView = request.view.bounds.height
        
        let minX = CGFloat(request.xmin)
        let maxX = CGFloat(request.xmax)
        let pointCount: CGFloat = CGFloat(summarizedArray.count)
        let startIndex = Int((minX/request.view.bounds.width) * pointCount)
        let endIndex = Int((maxX/request.view.bounds.width) * pointCount)
        let safeStartIndex = startIndex >= 0 ? startIndex : 0
        let safeEndIndex = Int(endIndex) < Int(pointCount) ? Int(endIndex) : Int(pointCount)
        
        let reducedArrays = chart.ticks.map { $0.yArray[safeStartIndex..<safeEndIndex] }
        
        guard let rCount = reducedArrays.first?.count else { return }
        let widthCoef = request.view.bounds.width / CGFloat(rCount)
        
        for row in chart.ticks.enumerated() {
            var percArray = [CGPoint]()
            var shareArray = [CGFloat]()
            
            for i in 0..<summarizedArray.count {
                // raw array
                let percY = CGFloat(row.element.yArray[i]) / CGFloat(summarizedArray[i])
                shareArray.append(percY)
                // point array
                let point = CGPoint(x: row.element.data[i].x, y: CGFloat(percY * heightView))
                percArray.append(point)
            }
            let selectedArray = Array(percArray[safeStartIndex..<safeEndIndex])
            
            var finalArray = [CGPoint]()
            for (index, point) in selectedArray.enumerated() {
                let newPoint = CGPoint(x: CGFloat(index) * widthCoef, y: point.y)
                finalArray.append(newPoint)
            }
        
            var axData = AxisData(name: chart.ticks[row.offset].name, yArray: chart.ticks[row.offset].yArray, data: finalArray)
            axData.shareArray = shareArray
            pieData.append(axData)
        }
        
        // sumForPie получается из выбранных данных по массиву yArray - от xmin до xmax по шкале bottomView
        let sumForPie = reducedArrays.map{$0.reduce(0, +)}
        
        let percentageForPie: [CGFloat] = sumForPie.map{(CGFloat($0) / CGFloat(sumForPie.reduce(0, +)))}
        
        var chartForPieChart = Chart(name: chart.name, type: chart.type, colors: chart.colors, ticks: pieData, stacked: chart.stacked, y_scaled: chart.y_scaled, percentage: chart.percentage)
        chartForPieChart.percentageForPie = percentageForPie
        chartForPieChart.yLabelsText = makeMarksForYLabels(chartNumber: request.chartNumber)
        chartForPieChart.xLabelsText = createXLabelsDynamically(chartNumber: request.chartNumber, minX: CGFloat(request.xmin), maxX: CGFloat(request.xmax), viewWidth: request.view.bounds.size.width)
        completion(chartForPieChart)
    }
    
    func makeBarChartData(request: ChartRequest, completion: (Chart) -> ()) {
        guard let chart = makeChart(chart: request.chartNumber) else { return }
        var barChart = Chart(name: chart.name, type: chart.type, colors: chart.colors, ticks: chart.ticks, stacked: chart.stacked, y_scaled: chart.y_scaled, percentage: chart.percentage)
        
        var finalXLabels = [String]()
        let labels = self.makeMarksForXLabels(chartNumber: request.chartNumber)
        
        var startIndex = Int(CGFloat(request.xmin)/request.view.bounds.width * CGFloat(labels.count))
        var endIndex = Int(CGFloat(request.xmax)/request.view.bounds.width * CGFloat(labels.count))
        
        guard startIndex >= 0 else { startIndex = 0; return }
        guard endIndex <= labels.count else { endIndex = labels.count; return }
        let selectedLabels = Array(labels[startIndex..<endIndex])
        for label in selectedLabels {
            finalXLabels.append(label)
        }
        
        barChart.yLabelsText = makeMarksForYLabels(chartNumber: request.chartNumber)
        barChart.xLabelsText = finalXLabels
        completion(barChart)
    }
    
    /**
       Design experiments showed that 5 y-axis marks are just fine. There is no problem changing the return array count just by setting this parameter in Constants for ChartView where the label y-axis marks belong.
     - Parameter chartNumber: a chart number from request
     - top value for Y axis is either a power of 10 taken from a log10(top value), or 1/2 of the same power of 10 - just to avoid large empty spaces on a charting board
    */
    private func makeMarksForYLabels(chartNumber: Int) -> [String] {
        
        guard let axisData = ticksForLabels else {
            return ["0", "0", "0", "0", "0"]
        }
        
        var maxYvalues = [Float]()
        var minYvalues = [Float]()
        for yData in axisData {
            let maxYvalue = yData.yArray.max()!
            let minYvalue = yData.yArray.min()!
            maxYvalues.append(Float(maxYvalue))
            minYvalues.append(Float(minYvalue))
        }
        
        let topY = maxYvalues.max()!
        var maxY: Int = (Int)(ceilf(topY))
        //let minY: Float = minYvalues.min()!
        if maxY % 10 == 0 {
            if maxY > 1000000 {
                return [String(format: "%0.1f M", arguments: [(Float(maxY) / 1000000.0)]),
                        String(format: "%0.1f M", arguments: [(Float(maxY) / 1000000.0) * 3/4]),
                        String(format: "%0.1f M", arguments: [(Float(maxY) / 1000000.0) / 2]),
                        String(format: "%0.1f M", arguments: [(Float(maxY) / 1000000.0) / 4]),
                        "0.0"].reversed()
            } else if maxY > 1000 {
                
                return [String(format: "%0.1f K", arguments: [(Float(maxY) / 1000.0)]),
                        String(format: "%0.1f K", arguments: [(Float(maxY) / 1000.0) * 3/4]),
                        String(format: "%0.1f K", arguments: [(Float(maxY) / 1000.0) / 2]),
                        String(format: "%0.1f K", arguments: [(Float(maxY) / 1000.0) / 4]),
                        "0.0"].reversed()
            } else {
                return ["\(maxY)", "\(maxY * 3/4)", "\(maxY / 2)", "\(maxY / 4)", "0"]
            }
        } else {
            repeat {
                maxY += 1
            } while (maxY % 10 != 0)
            return ["\(maxY)", "\(maxY * 3/4)", "\(maxY / 2)", "\(maxY / 4)", "0"]
        }
    }
    
    private func makeMarksForXLabels(chartNumber: Int) -> [String] {
        var xLabels = [String]()
        if let column = rawcharts[chartNumber].dataColumns.first {
            xLabels = makeXaxisArrayLabelText(xColumn: column)
        }
        return xLabels
    }
    
    private func makeXaxisDate(interval: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(interval/1000))
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: date)
    }
    
    private func makeXaxisArrayLabelText(xColumn: [Column]) -> [String] {
        
        var lta = [String]()
        calculationQueue.addOperation {
            for value in xColumn {
                switch value {
                case .integer(let i):
                    let st = self.makeXaxisDate(interval: i)
                    lta.append(st)
                case .string(let _):
                    break
                }
            }
        }
        calculationQueue.waitUntilAllOperationsAreFinished()
        return lta
    }
}

extension DataPoints: ChartingViewDataSource {
    func provideDataForMonthlyChart(monthName: String) {
        month = monthName
    }
    
    
    func provideDataForChart(request: ChartRequest, response: (Chart) -> ()) {
        makeChartDataFor(request: request) { (chart) in
            response(chart)
        }
    }
    
    func provideDataForPieChart(request: ChartRequest, response: (Chart) -> ()) {
        makeChartDataForPie(request: request) { (chart) in
            response(chart)
        }
    }
    
    func provideDataForBarChart(request: ChartRequest, response: (Chart) -> ()) {
        makeBarChartData(request: request) { (chart) in
            response(chart)
        }
    }
}

extension DataPoints {
    func convertHexColorToUIColor(hexColor: String, alphaChannel: CGFloat) -> UIColor {
        var hexString: String = hexColor.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (hexString.hasPrefix("#")) {
            hexString.remove(at: hexString.startIndex)
        }
        
        if ((hexString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue: UInt32 = 0
        _ = Scanner(string: hexString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alphaChannel
        )
    }
}

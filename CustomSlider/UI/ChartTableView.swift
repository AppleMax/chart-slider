//
//  ChartTableView.swift
//  CustomSlider
//
//  Created by Maxim on 23.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit

class ChartTableView: UITableView {

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        register(ChartingCell.self, forCellReuseIdentifier: ChartingCell.reuseId)
        register(CommandCell.self, forCellReuseIdentifier: CommandCell.reuseId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



//
//  DataConverter.swift
//  CustomSlider
//
//  Created by Maxim on 16.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation
import UIKit

struct DataConverter {
    
    var charts: MaxTlgElement?
    private let conversionQueue = OperationQueue()
    
    init(chartFolder: String) {
        conversionQueue.qualityOfService = .userInitiated
        
        if let resourcePath = Bundle.main.path(forResource: "\(chartFolder)/overview", ofType: "json") {
            let fileURL = URL(fileURLWithPath: resourcePath)
            guard let tlgJsonData: Data = try? Data(contentsOf: fileURL) else { return }
            let decoder = JSONDecoder()
            do {
                charts = try decoder.decode(MaxTlgElement.self, from: tlgJsonData)
            } catch {
                print("DECODING ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    func fetchMonthData(folderName: String) -> [MaxTlg] {
        var chartJsonArray = [MaxTlg]()

        let fm = FileManager.default
        let folderPaths = Bundle.main.paths(forResourcesOfType: nil, inDirectory: folderName)
        conversionQueue.addOperation {
        folderPaths.forEach { folderPath in
            do {
                let items = try fm.contentsOfDirectory(atPath: folderPath)
                var monthlyData = [MaxTlgElement]()
                  items.forEach { item in
                    if let md = self.decodeDailyData(filePath: "\(folderPath)/\(item)") {
                        monthlyData.append(md)
                    }
                  }
                let monthName: String = String(folderPath.suffix(7))
                let maxTlg = MaxTlg(monthName: monthName, monthData: monthlyData)
                chartJsonArray.append(maxTlg)
            } catch {
                print("Error reading folder \(error.localizedDescription)")
                }
            }
        }
        return chartJsonArray
    }
    
    private func decodeDailyData(filePath: String) -> MaxTlgElement? {
        let fileURL = URL(fileURLWithPath: filePath)
        guard let tlgJsonData: Data = try? Data(contentsOf: fileURL) else { return nil }
        let decoder = JSONDecoder()
        guard let dailyData = try? decoder.decode(MaxTlgElement.self, from: tlgJsonData) else { return nil }
        return dailyData
    }
}


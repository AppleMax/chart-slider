//
//  ViewController+ChartingTableViewDelegate.swift
//  CustomSlider
//
//  Created by Maxim on 24.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                cell = ChartingCell(frame: CGRect.zero)
                cell = tableView.dequeueReusableCell(withIdentifier: ChartingCell.reuseId, for: indexPath) as! ChartingCell
                cell.setColorTheme(colorTheme: colorTheme)
                cell.selectionStyle = .none
                configStack(inView: cell.contentView)
            case 1:
                cell = UITableViewCell(style: .default, reuseIdentifier: CommandCell.reuseId)
                cell = tableView.dequeueReusableCell(withIdentifier: CommandCell.reuseId, for: indexPath)
                cell.setColorTheme(colorTheme: colorTheme)
                cell.textLabel?.text = "JOINED CHANNEL"
                cell.accessoryType = .checkmark
                let greenSquare = drawColorMark(color: ColorScheme.greenLineChart.night())
                cell.imageView?.layer.cornerRadius = 5
                cell.imageView?.clipsToBounds = true
                cell.imageView?.image = greenSquare
            case 2:
                cell = UITableViewCell(style: .default, reuseIdentifier: CommandCell.reuseId)
                cell = tableView.dequeueReusableCell(withIdentifier: CommandCell.reuseId, for: indexPath)
                cell.setColorTheme(colorTheme: colorTheme)
                cell.textLabel?.text = "LEFT CHANNEL"
                cell.accessoryType = .checkmark
                let redSquare = drawColorMark(color: ColorScheme.redLineChart.night())
                cell.imageView?.layer.cornerRadius = 5
                cell.imageView?.clipsToBounds = true
                cell.imageView?.image = redSquare
            default:
                break
            }
        } else {
            cell = UITableViewCell(style: .default, reuseIdentifier: CommandCell.reuseId)
            cell = tableView.dequeueReusableCell(withIdentifier: CommandCell.reuseId, for: indexPath)
            cell.setColorTheme(colorTheme: colorTheme)
            cell.textLabel?.text = colorSwitchText
            cell.textLabel?.textColor = ColorScheme.blueLineTooltipText.night()
            cell.contentMode = .center
            cell.textLabel?.textAlignment = .center
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        setChartCoordinates(inView: bottomView, xmin: 0, xmax: Int(bottomView.frame.size.width))
        setChartCoordinates(inView: chartView, xmin: Int(topView.frame.origin.x), xmax: Int(topView.frame.maxX))
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "FOLLOWERS"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
                    return 360
                } else if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
                    return 700
                }
            case 1, 2:
                return UITableView.automaticDimension
            default:
                return UITableView.automaticDimension
            }
        } else {
            return UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                tableView.deselectRow(at: indexPath, animated: false)
            case 1:
                if joinedChannelIsSelected {
                    tableView.cellForRow(at: indexPath)?.accessoryType = .none
                    joinedChannelIsSelected = false
                    toggleJoinedChannel(isOn: joinedChannelIsSelected, indexPath: indexPath)
                } else if !joinedChannelIsSelected {
                    tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
                    joinedChannelIsSelected = true
                    toggleJoinedChannel(isOn: joinedChannelIsSelected, indexPath: indexPath)
                }
                tableView.deselectRow(at: indexPath, animated: true)
            case 2:
                tableView.deselectRow(at: indexPath, animated: true)
            default:
                break
            }
        case 1:
            switch colorTheme {
            case .day:
                toggleColorTheme()
                tableView.setColorTheme(colorTheme: colorTheme)
                setNightColorTheme()
                setChartCoordinates(inView: bottomView, xmin: 0, xmax: Int(bottomView.frame.size.width))
                setChartCoordinates(inView: chartView, xmin: Int(topView.frame.origin.x), xmax: Int(topView.frame.maxX))
                
                if let ipfcm = indexPathForCheckmark {
                    if ipfcm.isPresent {
                        tableView.cellForRow(at: ipfcm.ip)?.accessoryType = .checkmark
                    } else {
                        tableView.cellForRow(at: ipfcm.ip)?.accessoryType = .none
                    }
                }
                tableView.deselectRow(at: indexPath, animated: true)
            case .night:
                toggleColorTheme()
                tableView.setColorTheme(colorTheme: colorTheme)
                setDayColorTheme()
                setChartCoordinates(inView: bottomView, xmin: 0, xmax: Int(bottomView.frame.size.width))
                setChartCoordinates(inView: chartView, xmin: Int(topView.frame.origin.x), xmax: Int(topView.frame.maxX))
                
                if let ipfcm = indexPathForCheckmark {
                    if ipfcm.isPresent {
                        tableView.cellForRow(at: ipfcm.ip)?.accessoryType = .checkmark
                    } else {
                        tableView.cellForRow(at: ipfcm.ip)?.accessoryType = .none
                    }
                }
                tableView.deselectRow(at: indexPath, animated: true)
            }
        default:
            break
        }
    }
    
    private func drawColorMark(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 16, height: 16)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    private func toggleJoinedChannel(isOn: Bool, indexPath: IndexPath) {
        indexPathForCheckmark = (indexPath, joinedChannelIsSelected)
        ChartingView.joinedChannelIsOn = isOn
        setChartCoordinates(inView: bottomView, xmin: 0, xmax: Int(bottomView.frame.size.width))
        setChartCoordinates(inView: chartView, xmin: Int(topView.frame.origin.x), xmax: Int(topView.frame.maxX))
    }
    
}

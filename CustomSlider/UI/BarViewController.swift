//
//  BarViewController.swift
//  CustomSlider
//
//  Created by Maxim on 11.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit

class BarViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var chartNumber: Int
    var chartName: ChartName
    
    var bottomView: BottomView!
    var topView: TopView!
    var chartView: ChartView!
    //var pieChartView: PieChartView!
    var barChartView: BarChartView!
    
    private var leftFlexibleView: FlexibleView!
    private var rightFlexibleView: FlexibleView!
    private var chartingTableView: ChartTableView!
    private var stackView: UIStackView!
    private var panDirectionGR: PanDirectionGestureRecognizer!
    private var chartRequest: ChartRequest?
    
    var colorTheme: ColorTheme = .night
    var joinedChannelIsSelected = true
    var indexPathForCheckmark: (ip: IndexPath, isPresent: Bool)?
    
    var colorSwitchText: String {
        switch colorTheme {
        case .day:
            return "Switch To Night Mode"
        case .night:
            return "Switch To Day Mode"
        }
    }
    
    init(chartNumber: Int, chartName: ChartName) {
        self.chartNumber = chartNumber
        self.chartName = chartName
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        chartingTableView = configTableView(colorTheme: colorTheme)
        layoutTableView()
        
            switch colorTheme {
            case .day:
                setDayColorTheme()
            case .night:
                setNightColorTheme()
            }
        title = "Statistics"
    }
    
    func toggleColorTheme() {
        switch colorTheme {
        case .day:
            colorTheme = .night
        case .night:
            colorTheme = .day
        }
    }
    
    func setDayColorTheme() {
        view.backgroundColor = ColorScheme.scrollBackground.day()
        navigationController?.navigationBar.backgroundColor = ColorScheme.scrollBackground.day()
        navigationController?.navigationBar.barTintColor = ColorScheme.scrollBackground.day()
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
    }
    
    func setNightColorTheme() {
        view.backgroundColor = ColorScheme.scrollBackground.night()
        navigationController?.navigationBar.backgroundColor = ColorScheme.scrollBackground.night()
        navigationController?.navigationBar.barTintColor = ColorScheme.scrollBackground.night()
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    func configStack(inView: UIView) {
        
        leftFlexibleView = configLeftFlexibleView()
        rightFlexibleView = configRightFlexibleView()
        bottomView = configBottomView(lflexibleView: leftFlexibleView)
        panDirectionGR = configPanRecognizer()
        panDirectionGR.delaysTouchesBegan = true
        topView.addGestureRecognizer(panDirectionGR)
        
        switch chartName {
        case .growth, .stacked:
            barChartView = configBarChartView()
            let views = [barChartView, bottomView]
            stackView = UIStackView(arrangedSubviews: views as! [UIView])
        default:
            chartView = configChartView()
            let lineDrawer = configPanForChartView()
            lineDrawer.delaysTouchesBegan = true
            barChartView.addGestureRecognizer(lineDrawer)
            let views = [barChartView, bottomView]
            stackView = UIStackView(arrangedSubviews: views as! [UIView])
        }
        
        inView.addSubview(stackView)
        
        layoutBottomView()
        layoutTopView()
        layoutLeftFlexibleView()
        layoutRightFlexibleView()
        switch chartName {
        case .growth, .stacked:
            layoutBarChartView()
        default:
            layoutChartView()
        }
        
        stackLayout(inView: inView)
    }
    
    private func configTableView(colorTheme: ColorTheme) -> ChartTableView {
        let tableView = ChartTableView(frame: view.bounds, style: .grouped)
        view.addSubview(tableView)
        tableView.isScrollEnabled = true
        tableView.bounces = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.autoresizesSubviews = true
        tableView.setColorTheme(colorTheme: colorTheme)
        return tableView
    }
    
    private func configChartView() -> ChartView {
        let frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height/1.7)
        let chview = ChartView(frame: frame, colorTheme: colorTheme, chartName: chartName)
        return chview
    }
    
    private func configBarChartView() -> BarChartView {
        let frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height/1.7)
        let chview = BarChartView(frame: frame, colorTheme: colorTheme, chartName: chartName)
        return chview
    }
    
    private func configBottomView(lflexibleView: UIView) -> BottomView {
        let frameBottom = CGRect(x: 0, y: view.bounds.maxY/2, width: view.bounds.width, height: 100)
        let bview = BottomView(frame: frameBottom, colorTheme: colorTheme, chartName: chartName)
        let frameTop = CGRect(x: bview.bounds.width * 5/6, y: 0, width: bview.bounds.width/6, height: bview.bounds.height)
        topView = TopView(frame: frameTop, colorTheme: colorTheme)
        topView.isOpaque = false
        topView.layer.cornerRadius = 12
        bview.insertSubview(lflexibleView, at: 0)
        bview.insertSubview(rightFlexibleView, at: 1)
        bview.addSubview(topView)
        view.addSubview(bview)
        return bview
    }
    
    private func configLeftFlexibleView() -> FlexibleView {
        let frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 100))
        let lfv = FlexibleView(frame: frame, colorTheme: colorTheme)
        return lfv
    }
    
    private func configRightFlexibleView() -> FlexibleView {
        let point = CGPoint(x: 200, y: 0)
        let frame = CGRect(origin: point, size: CGSize(width: 100, height: 100))
        let rfv = FlexibleView(frame: frame, colorTheme: colorTheme)
        return rfv
    }
    
    private func configPanRecognizer() -> PanDirectionGestureRecognizer {
        let pr = PanDirectionGestureRecognizer(direction: .horizontal, view: topView, target: self, action: #selector(moveTopView(_:)))
        pr.maximumNumberOfTouches = 1
        pr.minimumNumberOfTouches = 1
        pr.delegate = self
        return pr
    }
    
    // MARK: Ruling the ChartingView
    
    func setChartCoordinates(inView: ChartingView, xmin: Int, xmax: Int) {
        inView.setDataForRequest(view: inView, xmin: xmin, xmax: xmax, chartNum: chartNumber)
    }
    
    /**
     - if leftArrow is dragged leftward, it expands the topView until it encounters a left boundary of the bottomView;
     if leftArrow is dragged rightward, it narrows the topView until it reaches a limit, defined in Constants
     - topView is pinned to the right boundary of the bottomView - rightArrow does not work
     - topView is somewhere in the middle of the bottomView - rightArrow can resize the topView
     - topView is resized till the right end - rightArrow is switched off
    */
    
    @objc private func moveTopView(_ sender: UIPanGestureRecognizer) {
        
        guard let senderView = sender.view else { return }
        let touchPoint = sender.location(in: senderView)
        let leftArrow: CGRect = CGRect(x: 0.0, y: 0.0, width: Constants.sliderArrowWidth, height: senderView.bounds.height)
        let rightArrow: CGRect = CGRect(x: senderView.bounds.width - Constants.sliderArrowWidth, y: 0, width: Constants.sliderArrowWidth, height: senderView.bounds.height)
        
        if leftArrow.contains(touchPoint) {
            
            if (sender.velocity(in: bottomView).x < CGFloat(0)){
                if senderView.frame.minX <= bottomView.bounds.minX {
                    let finalPoint = CGPoint(x: senderView.frame.maxX/2,
                                             y:senderView.center.y)
                    sender.state = .cancelled
                    senderView.center = finalPoint
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                } else {
                    senderView.frame.size.width = senderView.frame.size.width + abs(sender.translation(in: bottomView).x)
                    senderView.frame.origin.x = senderView.frame.origin.x + sender.translation(in: bottomView).x
                    leftFlexibleView.frame.size.width = senderView.frame.origin.x > 0 ? senderView.frame.origin.x : 0
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                }
            } else if (sender.velocity(in: bottomView).x > CGFloat(0)) {
                if senderView.frame.size.width <= CGFloat(Constants.topViewWidth) {
                    senderView.frame.size.width = CGFloat(Constants.topViewWidth)
                    senderView.frame.origin.x = senderView.frame.origin.x + sender.translation(in: bottomView).x
                    sender.state = .cancelled
                    leftFlexibleView.frame.size.width = senderView.frame.origin.x
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                } else {
                    senderView.frame.size.width -= sender.translation(in: bottomView).x
                    senderView.frame.origin.x = senderView.frame.origin.x + sender.translation(in: bottomView).x
                    leftFlexibleView.frame.size.width = senderView.frame.origin.x
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                }
            }
        } else if rightArrow.contains(touchPoint) {
            
            if (sender.velocity(in: bottomView).x > CGFloat(0)) {
                if senderView.frame.maxX >= bottomView.bounds.maxX {
                    let finalPoint = CGPoint(x: bottomView.bounds.width - senderView.frame.size.width/2,
                                             y:senderView.center.y)
                    sender.state = .cancelled
                    senderView.center = finalPoint
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                } else {
                    senderView.frame.size.width = senderView.frame.size.width + sender.translation(in: bottomView).x
                    
                    rightFlexibleView.frame.size.width = bottomView.bounds.width - senderView.frame.size.width - leftFlexibleView.frame.size.width
                    rightFlexibleView.frame.origin.x = senderView.frame.maxX
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                }
            } else if (sender.velocity(in: bottomView).x < CGFloat(0)) {
                if Int(senderView.frame.size.width) <= Int(Constants.topViewWidth) {
                    senderView.frame.size.width = CGFloat(Constants.topViewWidth)
                    sender.state = .cancelled
                    rightFlexibleView.frame.size.width = bottomView.bounds.width - senderView.frame.size.width - leftFlexibleView.frame.size.width
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                } else {
                    senderView.frame.size.width = senderView.frame.size.width + sender.translation(in: bottomView).x
                    rightFlexibleView.frame.origin.x = senderView.frame.maxX
                    rightFlexibleView.frame.size.width = bottomView.bounds.width - senderView.frame.size.width - leftFlexibleView.frame.size.width
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
                }
            }
        } else if !(leftArrow.contains(touchPoint) && rightArrow.contains(touchPoint)) {
            switch sender.state {
            case .began, .changed:
                if (sender.velocity(in: bottomView).x != CGFloat(0)) {
                    
                    senderView.frame.origin.x = senderView.frame.origin.x + sender.translation(in: bottomView).x
                    leftFlexibleView.frame.size.width = senderView.frame.origin.x > 0 ?
                        senderView.frame.origin.x : 0
                    rightFlexibleView.frame.size.width = (bottomView.bounds.width - senderView.frame.size.width - leftFlexibleView.frame.size.width) > 0 ? (bottomView.bounds.width - senderView.frame.size.width - leftFlexibleView.frame.size.width) : 0
                    rightFlexibleView.frame.origin.x = senderView.frame.maxX
                    sender.setTranslation(CGPoint.zero, in: bottomView)
                    
                    if (sender.velocity(in: bottomView).x < CGFloat(0)) {
                        if senderView.frame.minX <= bottomView.bounds.origin.x {
                            sender.state = .cancelled
                            let finalPoint = CGPoint(x: senderView.frame.size.width/2,
                                                     y:senderView.center.y)
                            senderView.center = finalPoint
                            sender.setTranslation(CGPoint.zero, in: bottomView)
                        } else {
                            sender.setTranslation(CGPoint.zero, in: bottomView)
                        }
                    } else if (sender.velocity(in: bottomView).x > CGFloat(0)) {
                        if senderView.frame.maxX >= bottomView.bounds.width {
                            sender.state = .cancelled
                            let finalPoint = CGPoint(x: bottomView.bounds.width - senderView.frame.size.width/2,
                                                     y:senderView.center.y)
                            senderView.center = finalPoint
                            sender.setTranslation(CGPoint.zero, in: self.bottomView)
                        } else {
                            sender.setTranslation(CGPoint.zero, in: bottomView)
                        }
                    }
                }
            case .ended, .cancelled:
                setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
            case .possible:
                setChartCoordinates(inView: barChartView, xmin: Int(senderView.frame.minX), xmax: Int(senderView.frame.maxX))
            case .failed:
                break
            }
        }
    }

    // MARK: GestureRecognizerDelegate
    
    private func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    // MARK: ChartView draw lines
    
    private func configPanForChartView() -> UIGestureRecognizer {
        let tapper = PanDirectionGestureRecognizer(direction: .horizontal, view: barChartView, target: self, action: #selector(drawLine(_:)))
        return tapper
    }
    
    @objc private func drawLine(_ sender: PanDirectionGestureRecognizer) {
        let touchPoint = sender.location(in: barChartView)
        if (sender.velocity(in: barChartView).x != CGFloat(0)) {
            barChartView.touchPoint = touchPoint
        }
    }
    
    // MARK: Layouts
    
    private func layoutTableView() {
        chartingTableView.translatesAutoresizingMaskIntoConstraints = false
        chartingTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        chartingTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        chartingTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        chartingTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func stackLayout(inView: UIView) {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.autoresizesSubviews = true
        
        stackView.topAnchor.constraint(equalTo: inView.topAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: inView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: inView.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: inView.bottomAnchor).isActive = true
    }
    
    private func layoutChartView() {
        barChartView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        barChartView.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        barChartView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        barChartView.heightAnchor.constraint(equalTo: bottomView.heightAnchor, multiplier: 5.0).isActive = true
    }
    
    private func layoutBarChartView() {
        barChartView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        barChartView.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        barChartView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        barChartView.heightAnchor.constraint(equalTo: bottomView.heightAnchor, multiplier: 5.0).isActive = true
    }
    
    private func layoutTopView() {
        topView.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor).isActive = true
        topView.leadingAnchor.constraint(greaterThanOrEqualTo: bottomView.leadingAnchor).isActive = true
        topView.widthAnchor.constraint(equalToConstant: CGFloat(Constants.topViewWidth)).isActive = true
        topView.heightAnchor.constraint(equalTo: bottomView.heightAnchor, multiplier: 1.02).isActive = true
        topView.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
    }
    
    private func layoutBottomView() {
        bottomView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        bottomView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
    }
    
    private func layoutLeftFlexibleView() {
        leftFlexibleView.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor).isActive = true
        leftFlexibleView.trailingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        leftFlexibleView.heightAnchor.constraint(equalTo: bottomView.heightAnchor).isActive = true
        leftFlexibleView.setContentCompressionResistancePriority(.init(750), for: .horizontal)
    }
    
    private func layoutRightFlexibleView() {
        rightFlexibleView.leadingAnchor.constraint(equalTo: topView.trailingAnchor).isActive = true
        rightFlexibleView.heightAnchor.constraint(equalTo: bottomView.heightAnchor).isActive = true
        rightFlexibleView.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor).isActive = true
    }
}


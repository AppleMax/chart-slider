//
//  Filenames.swift
//  CustomSlider
//
//  Created by Maxim on 08.04.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation

enum ChartName {
    case followers
    case interactions
    case growth
    case stacked
    case pie
    
    func fetchMonthFoldersPath() -> String {
        
        switch self {
        case .followers:
            return "1"
        case .interactions:
            return "2"
        case .growth:
            return "3"
        case .stacked:
            return "4"
        case .pie:
            return "5"
        }
    }
}

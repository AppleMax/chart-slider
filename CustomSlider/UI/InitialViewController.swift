//
//  InitialViewController.swift
//  CustomSlider
//
//  Created by Maxim on 06.04.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    
    @IBOutlet weak var oneButton: UIButton! {
        didSet {
            oneButton.setImage(UIImage(named: "linechart"), for: .normal)
            oneButton.backgroundColor = UIColor.white
            oneButton.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var twoButton: UIButton! {
        didSet {
            twoButton.setImage(UIImage(named: "linechart"), for: .normal)
            twoButton.backgroundColor = UIColor.white
            twoButton.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var threeButton: UIButton! {
        didSet {
            threeButton.setImage(UIImage(named: "barchart"), for: .normal)
            threeButton.backgroundColor = UIColor.white
            threeButton.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var fourButton: UIButton! {
        didSet {
            fourButton.setImage(UIImage(named: "barchart"), for: .normal)
            fourButton.backgroundColor = UIColor.white
            fourButton.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var fiveButton: UIButton! {
        didSet {
            fiveButton.setImage(UIImage(named: "piechart"), for: .normal)
            fiveButton.backgroundColor = UIColor.white
            fiveButton.layer.cornerRadius = 12
        }
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {

        switch sender.tag {
        case 101:
            let vc1 = ViewController(chartNumber: 0, chartName: .followers)
                navigationController?.pushViewController(vc1, animated: true)
        case 102:
            let vc2 = ViewController(chartNumber: 0, chartName: .interactions)
            navigationController?.pushViewController(vc2, animated: true)
        case 103:
            let vc3 = BarViewController(chartNumber: 0, chartName: .growth)
            navigationController?.pushViewController(vc3, animated: true)
        case 104:
            let vc4 = BarViewController(chartNumber: 0, chartName: .stacked)
            navigationController?.pushViewController(vc4, animated: true)
        case 105:
            let vc5 = PieViewController(chartNumber: 0, chartName: .pie)
            navigationController?.pushViewController(vc5, animated: true)
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorScheme.scrollBackground.night()
    }
}

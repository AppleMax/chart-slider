//
//  TopView.swift
//  CustomSlider
//
//  Created by Maxim on 12.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit

class TopView: UIView {
    
    var sliderFrameColor: UIColor!

    init(frame: CGRect, colorTheme: ColorTheme) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        layer.borderWidth = 4
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        contentMode = .redraw
        
        switch colorTheme {
        case .day:
            layer.borderColor = ColorScheme.scrollSelector.day().cgColor
            sliderFrameColor = ColorScheme.scrollSelector.day()
        case .night:
            layer.borderColor = ColorScheme.scrollSelector.night().cgColor
            sliderFrameColor = ColorScheme.scrollSelector.night()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        
        sliderFrameColor.setFill()
        UIColor.clear.setStroke()
        
        let rectLeft = CGRect(x: 0, y: 0, width: 27, height: rect.size.height)
        let cornerSize = CGSize(width: 12, height: 12)
        let path = UIBezierPath(roundedRect: rectLeft, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: cornerSize)
        path.move(to: CGPoint(x: 27, y: rect.size.height/6))
        path.addLine(to: CGPoint(x: 4, y: rect.size.height/2))
        path.addLine(to: CGPoint(x: 27, y: rect.size.height * 5/6))
        path.close()
        path.fill()
        path.stroke()
        
        let rectRight = CGRect(x: rect.maxX - 27, y: 0, width: 27, height: rect.size.height)
        let cornerSizeR = CGSize(width: 12, height: 12)
        let pathR = UIBezierPath(roundedRect: rectRight, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: cornerSizeR)
        pathR.move(to: CGPoint(x: rect.size.width - 27, y: rect.size.height/6))
        pathR.addLine(to: CGPoint(x: rect.size.width - 4, y: rect.size.height/2))
        pathR.addLine(to: CGPoint(x: rect.size.width - 27, y: rect.size.height * 5/6))
        pathR.close()
        pathR.usesEvenOddFillRule = true
        pathR.fill()
        pathR.stroke()
        
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = 4
    }
}

//
//  Chart.swift
//  CustomSlider
//
//  Created by Maxim on 20.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation
import UIKit

struct RawChart {
    let dataColumns: [[Column]]
    let name: Names
    let color: Colors
    let type: Types
    var stacked: Bool?
    var y_scaled: Bool?
    var percentage: Bool?
    
    init(data: [[Column]], name: Names, color: Colors, type: Types, id: Int, stacked: Bool?, y_scaled: Bool?, percentage: Bool?) {
        self.dataColumns = data
        self.name = name
        self.color = color
        self.type = type
        self.stacked = stacked
        self.y_scaled = y_scaled
        self.percentage = percentage
    }
}

struct Chart {
    let name: Names
    let type: Types
    let colors: [ChartColor]
    var ticks: [AxisData]
    var yLabelsText: [String]?
    var xLabelsText: [String]?
    var heightCoefForChart: CGFloat?
    var stacked: Bool?
    var y_scaled: Bool?
    var percentage: Bool?
    var percentageForPie: [CGFloat]?
    
    init(name: Names, type: Types, colors: [ChartColor], ticks: [AxisData], stacked: Bool?, y_scaled: Bool?, percentage: Bool?) {
        self.name = name
        self.type = type
        self.colors = colors
        self.ticks = ticks
        self.stacked = stacked
        self.y_scaled = y_scaled
        self.percentage = percentage
    }
}

struct AxisData {
    let name: String
    let yArray: [Int]
    var data: [CGPoint]
    var shareArray: [CGFloat]? 
    
    init(name: String, yArray: [Int], data: [CGPoint]) {
        self.name = name
        self.yArray = yArray
        self.data = data
    }
}

struct ChartColor {
    let name: String
    let color: UIColor
}

struct ChartRequest {
    let view: ChartingView
    let xmin: Int
    let xmax: Int
    let chartNumber: Int
    
    init(view: ChartingView, xmin: Int, xmax: Int, chartNum: Int) {
        self.view = view
        self.xmin = xmin
        self.xmax = xmax
        self.chartNumber = chartNum
    }
}

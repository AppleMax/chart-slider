//
//  TelegData.swift
//  CustomSlider
//
//  Created by Maxim on 16.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.

import Foundation

struct MaxTlg {
    let month: String
    let monthData: [MaxTlgElement]
    
    init(monthName: String, monthData: [MaxTlgElement]) {
        self.month = monthName
        self.monthData = monthData
    }
}

struct MaxTlgElement: Codable {
    let columns: [[Column]]
    let colors: Colors
    let types: Types
    let names: Names
    let percentage: Bool?
    let stacked: Bool?
    let y_scaled: Bool?
}

struct Types: Codable {
    let y0, x: String
    let y1, y2, y3, y4, y5, y6: String?
}

struct Names: Codable {
    let y0: String
    let y1, y2, y3, y4, y5, y6, x: String?
}

struct Colors: Codable {
    let y0: String
    let y1, y2, y3, y4, y5, y6, x: String?
}

enum Column: Codable {
    case integer(Int)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(Column.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Column"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

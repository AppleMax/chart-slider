//
//  BottomView.swift
//  CustomSlider
//
//  Created by Maxim on 12.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit

class BottomView: ChartingView {
    
    override init(frame: CGRect, colorTheme: ColorTheme, chartName: ChartName) {
        
        super.init(frame: frame, colorTheme: colorTheme, chartName: chartName)
        layer.borderWidth = 4
        translatesAutoresizingMaskIntoConstraints = false
        autoresizesSubviews = true
        contentMode = .redraw
        
        switch colorTheme {
        case .day:
            layer.borderColor = ColorScheme.scrollSelector.day().cgColor
            backgroundColor = ColorScheme.scrollBackground.day()
        case .night:
            layer.borderColor = ColorScheme.scrollSelector.night().cgColor
            backgroundColor = ColorScheme.scrollBackground.night()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        drawChart()
    }
    
    override func drawChart() {
        super.drawChart()
        guard let request = chartRequest else { return }
        dataSource?.provideDataForPieChart(request: request) {
            chart in
            colors = chart.colors
            
            if chart.type.y0 == "area" {
                
                var shareArrays = [[CGFloat]]()
                for dataRow in chart.ticks.enumerated() {
                    if let a = dataRow.element.shareArray {
                    shareArrays.append(a)
                    } else {
                        print("share array is empty")
                    }
                }
                
                var newSharedArrays = [[CGFloat]]()
                var tempArray = shareArrays[0]
                newSharedArrays.append(tempArray)
                var i = 1
                repeat {
                    for sh in shareArrays[i].enumerated() {
                        tempArray[sh.offset] += sh.element
                    }
                    newSharedArrays.append(tempArray)
                    i += 1
                } while (i != shareArrays.count)
                
                
                var pointsForPath = [[CGPoint]]()
                for dataRow in chart.ticks.enumerated() {
                    var points = [CGPoint]()
                    for (index, point) in dataRow.element.data.enumerated() {
                        let newPoint = CGPoint(x: point.x, y: newSharedArrays[dataRow.offset][index] * self.bounds.height)
                        points.append(newPoint)
                    }
                    pointsForPath.append(points)
                }
                
                
            for row in pointsForPath.reversed().enumerated() {
                let initPoint = row.element.first!
                let path = UIBezierPath()
                path.move(to: CGPoint(x: 0, y: 0))
                path.addLine(to: initPoint)
                for (_, point) in row.element.enumerated() {
                        path.addLine(to: point)
                }
                path.addLine(to: CGPoint(x: self.bounds.width, y: 0))
                
                path.close()
                path.lineWidth = Constants.chartLineWidthForChartView
                path.lineJoinStyle = .round
                colors.reversed()[row.offset].color.setFill()
                path.fill()
            }
            }
        }
    }
}


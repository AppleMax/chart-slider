//
//  ChartingView.swift
//  CustomSlider
//
//  Created by Maxim on 19.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation
import UIKit

protocol ChartingViewDataSource {
    func provideDataForChart(request: ChartRequest, response: (Chart) -> ())
    func provideDataForPieChart(request: ChartRequest, response: (Chart) -> ())
    func provideDataForBarChart(request: ChartRequest, response: (Chart) -> ())
    func provideDataForMonthlyChart(monthName: String)
}

class ChartingView: UIView {
    
    static var joinedChannelIsOn: Bool = true
    var chartRequest: ChartRequest?
    
    var paths = [UIBezierPath]()
    var colors = [ChartColor]()
    var ypoints = [AxisData]()
    
    var xlabels = [UILabel]()
    var yLabelText = [String]()
    var xLabelText = [String]()
    
    var labelDate = UILabel()
    var labelChartOne = UILabel()
    var labelChartTwo = UILabel()
    
    var labelDateText: ((String) -> ())?
    var labelChartOneText: ((String) -> ())?
    var labelChartTwoText: ((String) -> ())?
    
    var labelTextColor = UIColor()
    var lineColor = UIColor()
    
    var heightCoefficient: CGFloat?
    var heightForChart: CGFloat {
        return (bounds.height - 12) 
    }
    
    var dataSource: ChartingViewDataSource?
    
    init(frame: CGRect, colorTheme: ColorTheme, chartName: ChartName) {
        super.init(frame: frame)
        dataSource = DataPoints(chartName: chartName)
        
        switch colorTheme {
        case .day:
            backgroundColor = ColorScheme.scrollBackground.day()
            layer.borderColor = ColorScheme.scrollSelector.day().cgColor
            labelTextColor = ColorScheme.axisTextChart.day()
            lineColor = ColorScheme.gridLines.day()
            
        case .night:
            backgroundColor = ColorScheme.scrollBackground.night()
            layer.borderColor = ColorScheme.scrollSelector.night().cgColor
            labelTextColor = ColorScheme.axisTextChart.night()
            lineColor = ColorScheme.gridLines.night()
        }
        
        labelDateText = { labelText in
            self.labelDate.text = labelText
            switch colorTheme {
            case .day:
                self.labelDate.textColor = ColorScheme.blueLineTooltipText.day()
            case .night:
                self.labelDate.textColor = ColorScheme.blueLineTooltipText.night()
            }
            self.labelDate.adjustsFontSizeToFitWidth = true
        }
        labelChartOneText = { labelText in
            self.labelChartOne.text = labelText
            self.labelChartOne.adjustsFontSizeToFitWidth = true
        }
        labelChartTwoText = { labelText in
            self.labelChartTwo.text = labelText
            self.labelChartTwo.adjustsFontSizeToFitWidth = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setDataForRequest(view: ChartingView, xmin: Int, xmax: Int, chartNum: Int) {
        chartRequest = ChartRequest(view: view, xmin: xmin, xmax: xmax, chartNum: chartNum)
        view.setNeedsDisplay()
    }
    
    func drawChart() {
        
        guard let req = chartRequest else { return }
        dataSource?.provideDataForChart(request: req) { chart in
            colors = chart.colors
            ypoints = chart.ticks
            var secondHeightCoefficient: CGFloat = 1.0
            heightCoefficient = chart.heightCoefForChart ?? 1.0
            
            if (chart.y_scaled == true && chart.name.y1 != nil) {
                let secondPoints = ypoints.filter { $0.name == "y1" }
                let maximumY = secondPoints.first?.data.max { $1.y > $0.y }!.y
                guard let maxY = maximumY else { print ("Error Y1"); return }
                secondHeightCoefficient = self.bounds.height * 0.8 / maxY
            }
            
            yLabelText = chart.yLabelsText ?? ["0000"]
            xLabelText = chart.xLabelsText ?? ["Jan 2000"]
            
            if chart.type.y0 == "line" && chart.type.y1 == "line" {
                
                for row in ypoints.enumerated() {
                    
                    let chartPoints = row.element.data
                    var pointsForChart = [CGPoint]()
                    if (chart.y_scaled == true && row.element.name == "y1") {
                        pointsForChart = chartPoints.map {
                            let point = CGPoint(x: $0.x, y: (heightForChart - $0.y * secondHeightCoefficient))
                            return point
                        }
                        ypoints[row.offset].data = pointsForChart
                    } else {
                        pointsForChart = chartPoints.map {
                            let point = CGPoint(x: $0.x, y: (heightForChart - $0.y * (heightCoefficient ?? 1.0)))
                            return point
                        }
                    }
                    
                    let initPoint = pointsForChart.first
                    let path = UIBezierPath()
                    path.move(to: initPoint!)
                    for point in pointsForChart {
                        path.addLine(to: point)
                    }
                    path.lineWidth = Constants.chartLineWidthForChartView
                    path.lineJoinStyle = .round
                    paths.append(path)
                    if (ChartingView.joinedChannelIsOn == false && row.offset == 0) {
                        UIColor.clear.setStroke()
                    } else {
                        colors[row.offset].color.setStroke()
                    }
                    path.stroke()
                }
            } else if chart.type.y0 == "bar" {
                let viewWidth = req.view.bounds.width
                let viewHeight = req.view.bounds.height
                let barQuantity: Int = Int(viewWidth / Constants.barWidth)
                dataSource?.provideDataForBarChart(request: req) {
                    chart in
                    var rectOrigin = CGPoint(x: 0, y: viewHeight/2)
                    for _ in 0..<barQuantity {
                        let bar = UIBezierPath(rect: CGRect(origin: rectOrigin, size: CGSize(width: Constants.barWidth, height: self.bounds.height - rectOrigin.y)))
                        bar.lineWidth = 0
                        UIColor.blue.setFill()
                        bar.fill()
                        bar.stroke()
                        rectOrigin.x += Constants.barWidth
                        rectOrigin.y = CGFloat(Int.random(in: (Int(viewHeight) / 5)...(Int(viewHeight) / 2)))
                    }
                }
            } 
        }
    }
}

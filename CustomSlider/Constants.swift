//
//  Constants.swift
//  CustomSlider
//
//  Created by Maxim on 18.03.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static var topViewWidth: CGFloat {
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
            return 100
        }
        return 144
    }
    static let sliderArrowWidth: CGFloat = 30
    static let chartLineWidthForChartView: CGFloat = 3
    static let chartLineWidthForBottomView: CGFloat = 1
    static let chartVerticalLine: CGFloat = 2
    static let chartHorizontalLine: CGFloat = 0.5
    static let yAxisLabelsCountForChart: Int = 5
    static let xAxisLabelHeight: CGFloat = 12
    static var xAxisLabelWidth: CGFloat {
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
            return 44
        }
        return 80
    }
    
    static let barWidth: CGFloat = 12
    
    static var fontForYLabel: CGFloat {
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
            return 10
        }
        return 14
    }
    
    static var widthForYLabel: CGFloat {
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
            return 24
        }
        return 48
    }
    
    static var heightForYLabel: CGFloat {
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
            return 16
        }
        return 24
    }
}

extension Collection where Element: Numeric {
    var total: Element { return reduce(0, +) }
}

extension Collection where Element: BinaryInteger {
    var average: Double {
        return isEmpty ? 0 : Double(total) / Double(count)
    }
}

extension Collection where Element: BinaryFloatingPoint {
    var average: Element {
        return isEmpty ? 0 : total / Element(count)
    }
}
